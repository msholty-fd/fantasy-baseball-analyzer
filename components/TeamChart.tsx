import React from "react";
import stats from "fixtures/zips_hitters.json";
import ReactTable from "react-table";

const firstRow = stats.shift();

interface IProps {
  team: string;
  players: IPlayerRow[];
}

function getColumns(headers: string[]) {
  return headers.map(Header => ({
    Header,
    width: 70,
    accessor: (row: any) =>
      isNaN(row[Header]) ? row[Header] : Number(row[Header]),
    id: Header
  }));
}

function TeamChart(props: IProps) {
  const { team, players } = props;
  const columns = getColumns(firstRow!.columns!);
  const playerStats = stats.filter((stat: any) =>
    players.find(player => player.Player === stat.Name)
  );
  return (
    <>
      <h1>{team}</h1>
      <ReactTable data={playerStats} columns={columns} />
    </>
  );
}

export default TeamChart;
