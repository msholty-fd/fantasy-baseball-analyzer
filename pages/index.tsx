import React, { useState } from "react";
import { Button, Upload, message, Icon } from "antd";
import { UploadChangeParam } from "antd/lib/upload";
import Table from "react-table";
import TeamChart from "../components/TeamChart";

interface FileReaderEventTarget extends EventTarget {
  result?: string;
}

interface FileReaderEvent extends ProgressEvent {
  target: FileReaderEventTarget | null;
}

const csvRowRegex = /,(?![^"]*"(?:(?:[^"]*"){2})*[^"]*$)/;

function getColumns(headers: string[]) {
  return headers.map(Header => ({
    Header,
    accessor: Header
  }));
}

function transformRow(row: string) {
  return row.split(csvRowRegex).map(cell => cell.split('"').join(""));
}

function fixName([name, ...rest]: string[]) {
  const [lastName, firstName] = name.split(",");
  return [`${firstName.trim()} ${lastName.trim()}`, ...rest];
}

function objectifyRow(row: string[], headers: string[]) {
  return row.reduce(
    (previousValue, currentValue, index) => ({
      ...previousValue,
      [headers[index]]: currentValue
    }),
    {}
  );
}

function handleInput(setData: (data: IPlayerRow[]) => void, setColumns: any) {
  return (file: FileReaderEvent) => {
    if (!file.target) {
      return message.error("Seems like you didn't upload a file?");
    }

    const result = file.target.result;
    if (!result) {
      return message.error("Seems like the file is empty?");
    }
    const data = result.split("\n");
    const firstRow = data.shift();
    if (!firstRow) {
      return message.error("Seems like the file doesn't have headers?");
    }
    const headers = transformRow(firstRow);
    setData(
      data
        .map(transformRow)
        .map(fixName)
        .map(row => objectifyRow(row, headers))
    );
    setColumns(getColumns(headers));
  };
}

function onChange(setData: (data: IPlayerRow[]) => void, setColumns: any) {
  return (info: UploadChangeParam) => {
    const file = info.file.originFileObj;
    if (!file) {
      return message.error("Seems like you didn't upload a file?");
    }
    const reader = new FileReader();
    reader.onload = handleInput(setData, setColumns);
    reader.readAsText(file);
  };
}

function Home() {
  const [data, setData] = useState<IPlayerRow[]>([]);
  const [columns, setColumns] = useState([{}]);
  const teams = [...new Set(data.map(player => player.Status))];
  return (
    <>
      <div>Welcome to fantasy baseball analyzer!</div>
      <Upload accept=".csv" onChange={onChange(setData, setColumns)}>
        <Button type="primary">
          <Icon type="upload" /> Import Your Team from CSV
        </Button>
      </Upload>
      {data.length ? <Table data={data} columns={columns} /> : null}
      {teams.map(team => (
        <TeamChart
          key={team}
          team={team}
          players={data.filter(player => player.Status === team)}
        />
      ))}
    </>
  );
}

export default Home;
