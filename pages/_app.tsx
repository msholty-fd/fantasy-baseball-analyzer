import React from "react";
import App, { Container } from "next/app";
import MainLayout from "layouts/main";
import "antd/dist/antd.css";
import "react-table/react-table.css";

class MyApp extends App {
  render() {
    const { Component, pageProps } = this.props;

    return (
      <Container>
        <MainLayout>
          <Component {...pageProps} />
        </MainLayout>
      </Container>
    );
  }
}

export default MyApp;
